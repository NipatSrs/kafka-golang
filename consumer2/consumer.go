package main

import (
	"context"
	"fmt"

	"github.com/Shopify/sarama"
)

type consumerHandler struct{}

func main() {
	broker := []string{"localhost:9092"}
	consumer, err := sarama.NewConsumerGroup(broker, "testgroup", nil)
	if err != nil {
		panic(err)
	}

	defer func() {
		if err := consumer.Close(); err != nil {
			panic(err)
		}
	}()
	var handler consumerHandler
	ctx := context.Background()
	for {
		err := consumer.Consume(ctx, []string{"workshop"}, handler)
		if err != nil {
			panic(err)
		}
	}

}

func (c consumerHandler) Setup(sarama.ConsumerGroupSession) error {
	return nil
}

func (c consumerHandler) Cleanup(sarama.ConsumerGroupSession) error {
	return nil
}

func (c consumerHandler) ConsumeClaim(se sarama.ConsumerGroupSession, claim sarama.ConsumerGroupClaim) error {
	for msg := range claim.Messages() {
		fmt.Printf("consume Message from topic %s partition %d offset %d Value %s\n", msg.Topic, msg.Partition, msg.Offset, msg.Value)
	}

	return nil
}
