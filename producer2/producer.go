package main

import (
	"fmt"
	"log"
	"time"

	"github.com/Shopify/sarama"
)

func main() {

	producer, err := sarama.NewAsyncProducer([]string{"localhost:9092"}, nil)
	if err != nil {
		panic(err)
	}

	defer func() {
		if err := producer.Close(); err != nil {
			log.Fatalln(err)
		}
	}()

	for {
		time.Sleep(2 * time.Second)
		select {
		case producer.Input() <- &sarama.ProducerMessage{Topic: "workshop", Value: sarama.StringEncoder("testing 123")}:
			fmt.Println("produce message ")
		case err := <-producer.Errors():
			log.Println("Failed to produce message", err)
		}

	}

}
