package main

import (
	"fmt"

	"github.com/Shopify/sarama"
)

func main() {
	broker := []string{"localhost:9092"}
	consumer, err := sarama.NewConsumer(broker, nil)
	if err != nil {
		panic(err)
	}

	partitionConsumer, err := consumer.ConsumePartition("workshop", 0, sarama.OffsetNewest)
	if err != nil {
		panic(err)
	}

	for msg := range partitionConsumer.Messages() {
		fmt.Printf("consume message from partition %d offset %d\n Value : %s\n", msg.Partition, msg.Offset, msg.Value)
	}

}
