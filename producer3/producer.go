package main

import (
	"fmt"
	"log"
	"time"

	"github.com/Shopify/sarama"
)

func main() {
	config := sarama.NewConfig()
	config.Producer.Return.Successes = true
	producer, err := sarama.NewAsyncProducer([]string{"localhost:9092"}, config)
	if err != nil {
		panic(err)
	}

	go func() {
		for range producer.Successes() {
			fmt.Println("producer success ")
		}
	}()

	go func() {
		for err := range producer.Errors() {
			log.Println(err)
		}
	}()

	for {
		time.Sleep(time.Second)
		message := &sarama.ProducerMessage{Topic: "workshop", Value: sarama.StringEncoder("testing 123")}
		producer.Input() <- message

	}

}
